const path = require('path')
const fs = require('fs-extra')
const { distDir } = require('./utils')

fs.removeSync(path.join(distDir, 'preview'))
