const path = require('path')
const fs = require('fs-extra')
const inquirer = require('inquirer')
const { log, srcDir, distDir, writeFile } = require('./utils')
const tpl = (json) => `<template lang="pug">
view yo
</template>

<style lang="scss">
@import 'variable';

</style>

<script lang="javascript">
const { redirect, navigate } = require('../libs/wechat')

Page({
  data: {
  },
  redirect, navigate
})
</script>

<template lang="json">
${JSON.stringify(json, null, 2)}
</template>
`

inquirer
  .prompt([
    {
      type: 'input', name: 'page', message: 'Page name, eg. my/adreess',
      validate(input) {
        if (!input || input === 'app') return false

        const inputPath = path.join(srcDir, input + '.vue')
        if (fs.existsSync(inputPath)) return 'Page already exists'
        return true
      },
      filter(input) {
        if (!input) return input

        return 'pages/' + input.trim().replace(/(^\/*|\/*$)/g, '')
      }
    },
    { type: 'input', name: 'navigationBarBackgroundColor', message: '导航栏背景颜色' },
    {
      type: 'list', name: 'navigationBarTextStyle', message: '导航栏标题颜色，仅支持 black / white',
      choices: [{ name: 'empty', value: '' }, 'black', 'white']
    },
    {
      type: 'input', name: 'navigationBarTitleText', message: '导航栏标题文字内容',
      validate(input) {
        return !!input
      }
    },
    {
      type: 'list', name: 'navigationStyle', message: '导航栏样式，仅支持 default / custom(只保留右上角胶囊按钮)',
      choices: [{ name: 'empty', value: '' }, 'default', 'custom']
    },
    { type: 'input', name: 'backgroundColor', message: '窗口的背景色' },
    {
      type: 'list', name: 'backgroundTextStyle', message: '下拉 loading 的样式，仅支持 dark / light',
      choices: [{ name: 'empty', value: '' }, 'dark', 'light']
    },
    { type: 'confirm', name: 'enablePullDownRefresh', message: '是否开启当前页面下拉刷新', default: false },
  ])
  .then(answers => {
    if (answers.enablePullDownRefresh === false) delete answers.enablePullDownRefresh
    for (const a in answers) {
      const answer = answers[a]
      if (answer === '') delete answers[a]
    }

    const { page, ...rest } = answers
    const pagePath = path.join(srcDir, page + '.vue')

    writeFile(pagePath, tpl(rest))
    log.info('create', page)

    // update  or create app.json
    // const appPath = path.join(distDir, 'app.json')
    // let app = null
    // try {
    //   app = require(appPath)
    // } catch (e) {
    //   app = { pages: [] }
    // }

    // app.pages.push(page + '/' + page.split('/').pop())

    // writeFile(appPath, JSON.stringify(app, null, 2))

    // log.info('update', appPath)
  })
