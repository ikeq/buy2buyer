const path = require('path')
const fs = require('fs-extra')
const chalk = require('chalk')
const sass = require('node-sass')
const pug = require('pug')

exports.srcDir = path.join(__dirname, '../src')
exports.distDir = path.join(__dirname, '../wx')
exports.writeFile = function (file, data) {
  fs.ensureFileSync(file)
  fs.writeFileSync(file, data)
}

const colors = {
  info: chalk.green,
  error: chalk.red,
  mute: chalk.hex('#aaa')
};
/**
 * @example
 * log.info('add', 'file.ext')
 */
exports.log = {
  info(tag, message) {
    this.log('info', message, tag)
  },
  error(tag, message) {
    this.log('error', message, tag)
  },
  mute(message) {
    this.log('mute', message)
  },
  log(type, message, tag) {
    const color = colors[type] || colors.info
    let msg = ''
    if (tag) msg += color(tag.toUpperCase())
    msg += ' ' + colors.mute(message)
    console.log(msg)
  }
}

exports.renderers = {
  scss(data) {
    return sass.renderSync({
      data,
      includePaths: [
        path.join(path.join(exports.srcDir, 'styles'))
      ],
      outputStyle: 'expanded',
      functions: {
        'base64($url)': function (url) {
          const imgPath = path.join(exports.distDir, url.getValue())
          let target = fs.readFileSync(imgPath)
          const base64 = Buffer.from(target).toString('base64');
          return sass.types.String(`url(data:image/png;base64,${base64})`);
        }
      }
    }).css
  },
  pug(data) {
    return pug.render(data, {
      // doctype: 'html',
      pretty: true
    }).trim()
  },
  javascript(data) {
    return this.raw(data)
  },
  json(data) {
    return this.raw(data)
  },
  raw(data) {
    return data.trim()
  }
}

const ignores = ['.DS_Store']
/**
 * @param {string} dir
 * @param {function} handler
 */
exports.walkFile = function (dir, handler) {
  const files = fs.readdirSync(dir)
  files.forEach(file => {
    if (~ignores.indexOf(file)) return
    const fullpath = path.join(dir, file)
    const stat = fs.statSync(fullpath)
    if (stat.isFile()) handler(fullpath)
    else if (stat.isDirectory) {
      exports.walkFile(fullpath, handler)
    }
  })
}
