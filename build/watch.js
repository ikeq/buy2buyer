const path = require('path')
const fs = require('fs-extra')
const chokidar = require('chokidar')

const { log, srcDir, distDir, renderers, writeFile, walkFile } = require('./utils')

const extMap = {
  scss: 'wxss',
  pug: 'wxml',
  javascript: 'js',
  json: 'json'
}
const routes = []

// copy public
fs.copySync(path.join(srcDir, 'public'), path.join(distDir))
// build
walkFile(srcDir, (eventPath) => {
  process('add', eventPath)
})
addRoutes()
// watch
chokidar.watch(srcDir, {
  persistent: true,
  ignoreInitial: true,
  awaitWriteFinish: {
    stabilityThreshold: 400,
    pollInterval: 1
  }
}).on('all', (event, eventPath) => process(event, eventPath, true))

function process(event, eventPath, isWatch) {
  const targetName = path.basename(eventPath)
  const targetPath = path.relative(srcDir, eventPath)
  const targetDir = path.dirname(targetPath)
  const targetExt = path.extname(eventPath)
  const targetNameSansExt = path.basename(targetName, targetExt)

  // deleting
  const isPublic = targetPath.startsWith('public')
  const inDistTargetPath = path.join(distDir, path.relative(isPublic ? 'public' : '', targetPath))
  if (event === 'unlink' || event === 'unlinkDir') {
    log.info(event, inDistTargetPath)
    if (fs.existsSync(inDistTargetPath)) fs.removeSync(inDistTargetPath)
  }
  // public copying
  else if (isPublic && isWatch) {
    // TODO:
    // log.error('error', 'conflict between ' + targetPath + ' and ' + '')
    log.info(event, inDistTargetPath)
    fs.copySync(eventPath, inDistTargetPath)
  }

  // trigger global style to process
  const isStyle = targetPath.startsWith('styles')
  if (isWatch && isStyle) return process('change', path.join(srcDir, 'app.vue'))
  if (targetExt !== '.vue') return

  switch (event) {
    case 'add':
    case 'change':
      const parsed = parse(fs.readFileSync(eventPath))
      if (Object.keys(parsed)) {
        for (const lang in parsed) {
          const outExt = extMap[lang]
          if (renderers[lang] && outExt) {
            let rendered = null
            try {
              rendered = renderers[lang](parsed[lang].join('\n\n'))
            } catch (e) {
              log.error('error', targetPath)
              log.mute(e.formatted || e.message)
              break;
            }

            const outPath = path.join(
              distDir,
              targetDir,
              // targetNameSansExt !== 'app' ? targetNameSansExt : '',
              targetNameSansExt + '.' +
              outExt
            )
            log.info(event, outPath)
            writeFile(outPath, rendered)
          }
        }
      }

      // new page
      if (event === 'add' && targetDir.startsWith('pages')) {
        routes.push((targetDir + '/' + targetNameSansExt).replace(/\\+/g, '/'))
        addRoutes()
        return
      }

      // change app.vue
      if (event === 'change' && targetPath === 'app.vue') addRoutes()
      break;

    case 'addDir':
    case 'unlink':
    case 'unlinkDir':
      break;
  }
}

// add route to app.json
function addRoutes() {
  const appJsonPath = path.join(distDir, 'app.json')
  let appJson = null
  try {
    appJson = require(appJsonPath)
  } catch (e) {
    appJson = { pages: [] }
  }
  appJson.pages = routes.sort((a, b) => {
    const partsA = a.split('/')
    const partsB = b.split('/')
    if (partsA[1] === 'index') return -1
    if (partsB[1] === 'index') return 1
    return (partsA.length - partsB.length) || (a.length - b.length)
  })
  log.info('change', appJsonPath)
  fs.writeFileSync(appJsonPath, JSON.stringify(appJson, null, 2))
}
function parse(str) {
  const regex = /\<(style|template|script) lang="(?<lang>\w*)"\>(?<code>[\s\S]*?)\<\/\1\>/gm;
  const codes = {}
  let m;

  while ((m = regex.exec(str)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === regex.lastIndex) regex.lastIndex++

    const { lang, code } = m.groups

    if (!codes[lang]) codes[lang] = []
    codes[lang].push(code.trim())
  }

  return codes
}
