exports.modal = function (content, options = {}) {
  if (typeof content === 'object') {
    options = content
    content = options.content
  }
  return new Promise(resolve => {
    wx.showModal({
      content: content,
      ...options,
      success: (res) => {
        options.success && options.success(res)
        if (res.confirm) resolve()
      }
    })
  })
}

exports.toast = function (msg, options = {}) {
  return new Promise(resolve => {
    wx.showToast({
      title: msg,
      ...options,
      success: (res) => {
        options.success && options.success(res)
        resolve()
      }
    })
  })
}

exports.navigate = function (e) {
  if (typeof e === 'string') {
    wx.navigateTo({ url: e })
  } else {
    const { to, tab } = e.currentTarget.dataset
    if (tab !== undefined) wx.switchTab({ url: to })
    else wx.navigateTo({ url: to })
  }

}
exports.redirect = function (e) {
  const to = typeof e === 'string' ? e : e.currentTarget.dataset.to
  wx.redirectTo({ url: to })
}

exports.debounce = function (func, wait) {
  return (...args) => {
    // const args = arguments;

    clearTimeout(func.debounce);
    func.debounce = setTimeout(() => {
      func(...args);
    }, wait);
  };
}
